#include <wiringpi2/wiringPi.h>

int main(void) {
	wiringPiSetup();
	for (;;) {
		// AIN.0 is physical pin 40, wPi pin 29
		int value = analogRead(29);
		printf("analog input: %d \n", value);
		delay(100);
	}
}
