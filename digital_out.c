#include <time.h>
#include <wiringpi2/wiringPi.h>


/*
 * Test code for turning digital outputs on and off.
 * Questions I wanted to answer:
 * 1) Can I control pins?
 * 2) Is it possible to read the status of an output pin?
 * 3) If so, how long does that call take?
 */


void printPinStates() {
	clock_t t0, t1;
	t0 = clock();
	// This is embarrassing. 
	// I really should figure out how to make an array of pins and iterate 
	// over it. I know modern C++, not C!
	int pin2 = digitalRead(2);
	int pin4 = digitalRead(4);
	int pin5 = digitalRead(5);
	int pin11 = digitalRead(11);
	printf("Pin 2 state: %d \n", pin2);
	printf("Pin 4 state: %d \n", pin4);
	printf("Pin 4 state: %d \n", pin5);
	printf("Pin 11 state: %d \n", pin11);
	t1 = clock();
	double dt = ((double) t1 - t0) / CLOCKS_PER_SEC;
	printf("Checking pin states took %0.2f nano seconds \n", 1000000*dt);
}

int main(void) {
	wiringPiSetup();
	// Pin 13 is wpi pin 2
	pinMode(2, OUTPUT);
	// Pin 16 is wpi pin 4
	pinMode(4, OUTPUT);
	// Pin 18 is wpi pin 5
	pinMode(5, OUTPUT);
	// Pin 26 is wpi pin 11
	pinMode(11, OUTPUT);

	for(;;) {
		digitalWrite(2, HIGH);
		digitalWrite(4, HIGH);
		digitalWrite(5, HIGH);
		digitalWrite(11, HIGH);
		printPinStates();
		delay(1000);
		digitalWrite(2, LOW);
		digitalWrite(4, LOW);
		digitalWrite(5, LOW);
		digitalWrite(11, LOW);
		printPinStates();
		delay(1000);
	}
}

