#include <wiringpi2/wiringPi.h>

int main(void) {

	wiringPiSetup();
	// Physical pin 32 is wpi pin 26
	pinMode(26, INPUT);
	for (;;) {
		int value = digitalRead(26);
		printf("digital input: %d \n", value);
		delay(100);
	}
}
